Download the Apple and Google datasets. There should 4 .csv files.
Make sure load_data.py and the .csv files are in the same directory:
```
ls
```
yields at least
```
AppleStore.csv
googleplaystore_user_reviews.csv
appleStore_description.csv
googleplaystore.csv
load_data.py
```
From the postgres root user, run:
```
psql -U <user> -f setup_final_project.sql 
```
Then run the load_data.py:
```
python3.6 load_data.py
```
When ran successfully, accessing the database should be
```
psql -U finalproject
```
where
```
finalproject=>\dt
                 List of relations
   Schema    |     Name     | Type  |    Owner     
-------------+--------------+-------+--------------
 finalschema | apps         | table | finalproject
 finalschema | descriptions | table | finalproject
 finalschema | info         | table | finalproject
 finalschema | ratings      | table | finalproject
 finalschema | reviews      | table | finalproject
(5 rows)
```